<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \OpenAI;

class TextImprovementController extends Controller
{
    public function improveText(Request $request)
    {
        $inputText = $request->input('text');
        // $openai = new OpenAI(config('openai.api_key'));
        $client = OpenAI::client(env('OPENAI_API_KEY'));;
        $response = $client->completions()->create([
            'model' => 'gpt-3.5-turbo-instruct',
            'prompt' => $inputText,
            'max_tokens' => 10,
        ]);
        
        return response()->json([
            'improved_text' => $response['choices'][0]['text'],
            // 'improved_text' => $response['choices'][0]['messages'],
        ]);
    }
}